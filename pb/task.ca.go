package pb

import (
	"context"

	. "github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/protoc-gen-caw/convert"
	"go.uber.org/cadence/activity"
	"go.uber.org/cadence/workflow"
)

var (
	_ = Empty{}
	_ = convert.JsonB{}
)

const (
	TasksCreateTaskActivity          = "/saastack.tasks.v1.Tasks/CreateTask"
	TasksGetTaskActivity             = "/saastack.tasks.v1.Tasks/GetTask"
	TasksGetTaskActivityActivity     = "/saastack.tasks.v1.Tasks/GetTaskActivity"
	TasksDeleteTaskActivity          = "/saastack.tasks.v1.Tasks/DeleteTask"
	TasksUpdateTaskActivity          = "/saastack.tasks.v1.Tasks/UpdateTask"
	TasksBatchGetTaskActivity        = "/saastack.tasks.v1.Tasks/BatchGetTask"
	TasksListTaskActivity            = "/saastack.tasks.v1.Tasks/ListTask"
	TasksListTasksByTitleActivity    = "/saastack.tasks.v1.Tasks/ListTasksByTitle"
	TasksListTasksByStatusActivity   = "/saastack.tasks.v1.Tasks/ListTasksByStatus"
	TasksListTasksByDatetimeActivity = "/saastack.tasks.v1.Tasks/ListTasksByDatetime"
	TasksAssignEmployeeCheckActivity = "/saastack.tasks.v1.Tasks/AssignEmployeeCheck"
	TasksListReportByDateActivity    = "/saastack.tasks.v1.Tasks/ListReportByDate"
)

func RegisterTasksActivities(cli TasksClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *CreateTaskRequest) (*Task, error) {
			res, err := cli.CreateTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksCreateTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTaskRequest) (*Task, error) {
			res, err := cli.GetTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksGetTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTaskActivityRequest) (*GetTaskActivityResponse, error) {
			res, err := cli.GetTaskActivity(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksGetTaskActivityActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *DeleteTaskRequest) (*Empty, error) {
			res, err := cli.DeleteTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksDeleteTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateTaskRequest) (*Task, error) {
			res, err := cli.UpdateTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksUpdateTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *BatchGetTaskRequest) (*BatchGetTaskResponse, error) {
			res, err := cli.BatchGetTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksBatchGetTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskRequest) (*ListTaskResponse, error) {
			res, err := cli.ListTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTasksByTitleRequest) (*ListTasksByTitleResponse, error) {
			res, err := cli.ListTasksByTitle(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTasksByTitleActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTasksByStatusRequest) (*ListTasksByStatusResponse, error) {
			res, err := cli.ListTasksByStatus(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTasksByStatusActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTasksByDatetimeRequest) (*ListTasksByDatetimeResponse, error) {
			res, err := cli.ListTasksByDatetime(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTasksByDatetimeActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *AssignEmployeeCheckRequest) (*AssignEmployeeCheckResponse, error) {
			res, err := cli.AssignEmployeeCheck(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksAssignEmployeeCheckActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListReportByDateRequest) (*ListReportByDateResponse, error) {
			res, err := cli.ListReportByDate(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListReportByDateActivity},
	)
}

// TasksActivitiesClient is a typesafe wrapper for TasksActivities.
type TasksActivitiesClient struct {
}

// NewTasksActivitiesClient creates a new TasksActivitiesClient.
func NewTasksActivitiesClient(cli TasksClient) TasksActivitiesClient {
	RegisterTasksActivities(cli)
	return TasksActivitiesClient{}
}

func (ca *TasksActivitiesClient) CreateTask(ctx workflow.Context, in *CreateTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksCreateTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) GetTask(ctx workflow.Context, in *GetTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksGetTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) GetTaskActivity(ctx workflow.Context, in *GetTaskActivityRequest) (*GetTaskActivityResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksGetTaskActivityActivity, in)
	var result GetTaskActivityResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) DeleteTask(ctx workflow.Context, in *DeleteTaskRequest) (*Empty, error) {
	future := workflow.ExecuteActivity(ctx, TasksDeleteTaskActivity, in)
	var result Empty
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) UpdateTask(ctx workflow.Context, in *UpdateTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksUpdateTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) BatchGetTask(ctx workflow.Context, in *BatchGetTaskRequest) (*BatchGetTaskResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksBatchGetTaskActivity, in)
	var result BatchGetTaskResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTask(ctx workflow.Context, in *ListTaskRequest) (*ListTaskResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskActivity, in)
	var result ListTaskResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTasksByTitle(ctx workflow.Context, in *ListTasksByTitleRequest) (*ListTasksByTitleResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTasksByTitleActivity, in)
	var result ListTasksByTitleResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTasksByStatus(ctx workflow.Context, in *ListTasksByStatusRequest) (*ListTasksByStatusResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTasksByStatusActivity, in)
	var result ListTasksByStatusResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTasksByDatetime(ctx workflow.Context, in *ListTasksByDatetimeRequest) (*ListTasksByDatetimeResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTasksByDatetimeActivity, in)
	var result ListTasksByDatetimeResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) AssignEmployeeCheck(ctx workflow.Context, in *AssignEmployeeCheckRequest) (*AssignEmployeeCheckResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksAssignEmployeeCheckActivity, in)
	var result AssignEmployeeCheckResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListReportByDate(ctx workflow.Context, in *ListReportByDateRequest) (*ListReportByDateResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListReportByDateActivity, in)
	var result ListReportByDateResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

const (
	ParentServiceValidateParentActivity      = "/saastack.tasks.v1.ParentService/ValidateParent"
	ParentServiceBatchValidateParentActivity = "/saastack.tasks.v1.ParentService/BatchValidateParent"
)

func RegisterParentServiceActivities(cli ParentServiceClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
			res, err := cli.ValidateParent(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ParentServiceValidateParentActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *BatchValidateParentRequest) (*BatchValidateParentResponse, error) {
			res, err := cli.BatchValidateParent(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ParentServiceBatchValidateParentActivity},
	)
}

// ParentServiceActivitiesClient is a typesafe wrapper for ParentServiceActivities.
type ParentServiceActivitiesClient struct {
}

// NewParentServiceActivitiesClient creates a new ParentServiceActivitiesClient.
func NewParentServiceActivitiesClient(cli ParentServiceClient) ParentServiceActivitiesClient {
	RegisterParentServiceActivities(cli)
	return ParentServiceActivitiesClient{}
}

func (ca *ParentServiceActivitiesClient) ValidateParent(ctx workflow.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceValidateParentActivity, in)
	var result ValidateParentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *ParentServiceActivitiesClient) BatchValidateParent(ctx workflow.Context, in *BatchValidateParentRequest) (*BatchValidateParentResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceBatchValidateParentActivity, in)
	var result BatchValidateParentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
