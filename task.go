package tasks

import (
	"context"
	"database/sql"
	"github.com/elgris/sqrl"
	sqlpx "github.com/srikrsna/sqlx/proto"
	activityLogPb "go.saastack.io/activity-log/pb"
	"go.saastack.io/chaku/errors"
	employee "go.saastack.io/employee/pb"
	"go.saastack.io/idutil"
	project "go.saastack.io/project/pb"
	"go.saastack.io/task/pb"
	"go.saastack.io/userinfo"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
)

type tasksServer struct {
	db            *sql.DB
	employeeCli employee.EmployeesClient
	taskStore pb.TaskStore
	taskBLoC  pb.TasksServiceTaskServerBLoC
	parentServer pb.ParentServiceClient
	*pb.TasksServiceTaskServerCrud
	actLogCli activityLogPb.ActivityLogsClient

}

func NewTasksServer(
	db *sql.DB,
	employeeCli employee.EmployeesClient,
	taskSt pb.TaskStore,
	parentServer pb.ParentServiceClient,
	actLogCli activityLogPb.ActivityLogsClient,

) pb.TasksServer {
	r := &tasksServer{
		db: db,
		parentServer: parentServer,
        employeeCli: employeeCli,
		taskStore: taskSt,
		actLogCli: actLogCli,
	}

	taskSC := pb.NewTasksServiceTaskServerCrud(taskSt, r)
	r.TasksServiceTaskServerCrud = taskSC

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *tasksServer) CreateTaskBLoC(ctx context.Context, in *pb.CreateTaskRequest) error {
	if _, err := s.parentServer.ValidateParent(ctx, &pb.ValidateParentRequest{Id: in.GetParent()}); err != nil {
		return err
	}

	// Check if task title already not exists
	condition := pb.TaskAnd{pb.TaskParentEq{Parent: idutil.GetId(in.GetParent())}, pb.TaskTitleEq{Title: in.GetTask().GetTitle()}}

	_, err := s.taskStore.GetTask(ctx, []string{string(pb.Task_Id)}, condition)
	if err != nil {
		if err == errors.ErrNotFound {
			return nil
		}
		return err
	}

	return errors.ErrObjIdExist
}

func (s *tasksServer) GetTaskBLoC(ctx context.Context, in *pb.GetTaskRequest) error {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	return nil
}

func (s *tasksServer) UpdateTaskBLoC(ctx context.Context, in *pb.UpdateTaskRequest) error {
	//Validate Update mask
	for _, m := range in.GetUpdateMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	// Check if task exists or not
	condition := pb.TaskAnd{pb.TaskParentEq{Parent: idutil.GetParent(in.GetTask().GetId())}, pb.TaskTitleEq{Title: in.GetTask().GetTitle()}, pb.TaskIdNotEq{Id: in.GetTask().GetId()}}

	_, err := s.taskStore.GetTask(ctx, []string{string(pb.Task_Id)}, condition)
	if err != nil {
		if err == errors.ErrNotFound {
			return nil
		}
		return err
	}

	return errors.ErrObjIdExist
}

func (s *tasksServer) DeleteTaskBLoC(ctx context.Context, in *pb.DeleteTaskRequest) error {
	return nil
}

func (s *tasksServer) BatchGetTaskBLoC(ctx context.Context, in *pb.BatchGetTaskRequest) error {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	return nil
}
//List Tasks Project Wise
func (s *tasksServer) ListTaskBLoC(ctx context.Context, in *pb.ListTaskRequest) (pb.TaskCondition, error) {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	projectId := in.Parent
	cond := pb.TaskAnd{
		pb.TaskParentEq{Parent: idutil.GetId(projectId)}}

	return cond, nil
}

// These functions are not implemented by CRUDGen, needed to be implemented

func (s *tasksServer) ListTasksByTitle(ctx context.Context, in *pb.ListTasksByTitleRequest) (*pb.ListTasksByTitleResponse, error) {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}


	tasks, err := s.taskStore.ListTasks(ctx,
		s.GetViewMask(in.ViewMask),
		pb.TaskTitleEq{
			Title: in.GetTitle(),
		},
	)
	if err != nil {
		return nil, err
	}

	return &pb.ListTasksByTitleResponse{Task: tasks}, err

}

func (s *tasksServer) ListTasksByStatus(ctx context.Context, in *pb.ListTasksByStatusRequest) (*pb.ListTasksByStatusResponse, error) {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}


	tasks, err := s.taskStore.ListTasks(ctx,
		s.GetViewMask(in.ViewMask),
		pb.TaskStatusEq{
			Status: in.GetStatus(),
		},
	)
	if err != nil {
		return nil, err
	}

	return &pb.ListTasksByStatusResponse{Task: tasks}, err
}

func (s *tasksServer) ListTasksByDatetime(ctx context.Context, in *pb.ListTasksByDatetimeRequest) (*pb.ListTasksByDatetimeResponse, error) {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}


	tasks, err := s.taskStore.ListTasks(ctx,
		s.GetViewMask(in.ViewMask),
		pb.TaskDatetimeEq{
			Datetime: in.GetDatetime(),
		},
	)
	if err != nil {
		return nil, err
	}

	return &pb.ListTasksByDatetimeResponse{Task: tasks}, err
}
func (s *tasksServer) AssignEmployeeCheck(ctx context.Context, in *pb.AssignEmployeeCheckRequest) (*pb.AssignEmployeeCheckResponse, error) {

	if _,err:= s.employeeCli.GetEmployee(ctx,&employee.GetEmployeeRequest{Id: in.GetEmployeeId()}); err != nil {
		return &pb.AssignEmployeeCheckResponse{Valid: false}, status.Error(codes.FailedPrecondition, "Employee Does Not Exist")

	}

	return &pb.AssignEmployeeCheckResponse{Valid: true}, nil
}
func (s *tasksServer) GetTaskActivity (ctx context.Context, in *pb.GetTaskActivityRequest) (*pb.GetTaskActivityResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	meta := &pb.MetaInfo{}
	_, err := s.taskStore.GetTask(ctx,
		s.GetViewMask(in.ViewMask),
		pb.TaskIdEq{
			Id: in.GetId(),
		},meta,
	)
	if err != nil {
		return nil, err
	}

	return &pb.GetTaskActivityResponse{CreatedOn: &meta.CreatedOn,UpdatedOn: &meta.UpdatedOn}, err

}
func (s *tasksServer) ListReportByDate(ctx context.Context, in *pb.ListReportByDateRequest) (*pb.ListReportByDateResponse, error) {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	sel := sqrl.Select("DATE(task.created_on)", "count(DATE(task.created_on))").
		From("saastack_tasks_v1.task").
		Where(sqrl.And{
			sqrl.GtOrEq{"task.created_on": sqlpx.Timestamp(in.GetStartTime())},
			sqrl.LtOrEq{"task.created_on": sqlpx.Timestamp(in.GetEndTime())},
			sqrl.Eq{"task.status":true},
		}).
		GroupBy("DATE(task.created_on)").
		OrderBy("DATE(task.created_on)")

	query, args, err := sel.PlaceholderFormat(sqrl.Dollar).ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	list,err:=getData(rows)
	if err != nil {
		return nil, err
	}

	var totalTasksCompleted int64 = 0
	for _ , v :=range list{
		totalTasksCompleted += v.GetTotalTasksPerDay()
	}

	return &pb.ListReportByDateResponse{Report: list,TotalTasks : totalTasksCompleted},nil
}
func getData(rows *sql.Rows) ([]*pb.Report,error) {

	defer rows.Close()

	list := make([]*pb.Report, 0)

	for rows.Next() {
		rep := &pb.Report{}
		if err := rows.Scan(
			&rep.Date ,
			&rep.TotalTasksPerDay); err != nil {
			return nil, err
		}
		list = append(list, rep)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return list, nil
}

type parentServiceServer struct {
	proCli project.ProjectsClient
}

//NewParentServiceServer returns a ParentServiceServer implementation with core business logic
func NewParentServiceServer(proCli project.ProjectsClient) pb.ParentServiceServer {
	return &parentServiceServer{
	  proCli: proCli,
	}

}

// These functions are not implemented by CRUDGen, needed to be implemented
func (s *parentServiceServer) ValidateParent(ctx context.Context, in *pb.ValidateParentRequest) (*pb.ValidateParentResponse, error) {
	if skip := userinfo.SkipParent(ctx); !skip {
		if _, err := s.proCli.GetProject(ctx, &project.GetProjectRequest{Id: in.GetId(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}}); err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}
	}

	return &pb.ValidateParentResponse{Valid: true}, nil

}

func (s *parentServiceServer) BatchValidateParent(ctx context.Context, in *pb.BatchValidateParentRequest) (*pb.BatchValidateParentResponse, error) {
	if skip := userinfo.SkipParent(ctx); !skip {
		batchRes, err := s.proCli.BatchGetProject(ctx, &project.BatchGetProjectRequest{Ids: in.GetIds(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}})
		if err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}
		for _, b := range batchRes.GetProject() {
			if b == nil {
				return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
			}
		}
	}

	return &pb.BatchValidateParentResponse{Valid: true}, nil
}


