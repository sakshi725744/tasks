package tasks

import (
	"context"
	"strings"
	"github.com/golang/protobuf/ptypes/empty"
	activityLog "go.saastack.io/activity-log"
	activityLogPb "go.saastack.io/activity-log/pb"
	"go.saastack.io/idutil"
	"go.saastack.io/task/pb"

)

type logsTasksServer struct {
	pb.TasksServer
	actLogCli activityLogPb.ActivityLogsClient
	str       pb.TaskStore
}

func NewLogsTasksServer(
	a activityLogPb.ActivityLogsClient,
	s pb.TasksServer,
	str pb.TaskStore,
) pb.TasksServer {

	srv := &logsTasksServer{s, a,str}

	return srv
}


func (s *logsTasksServer) CreateTask(ctx context.Context, in *pb.CreateTaskRequest) (*pb.Task, error) {

	res, err := s.TasksServer.CreateTask(ctx, in)
	if err != nil {
		return nil, err
	}


	task := &pb.Task{}
	if err = task.Update(res, pb.TaskFields(
		pb.Task_Title,
		pb.Task_Status,
		pb.Task_Assignee ,
	)); err != nil {
		return nil, err
	}

	/*
		Template: {{title}} {{status}} ({{assignee}})
	*/
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          in.GetParent(),
		Data:            task,
		EventName:       ".saastack.tasks.v1.Tasks.CreateTask",
		ActivityId:      res.Id,
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil



}

func (s *logsTasksServer) GetTask(ctx context.Context, in *pb.GetTaskRequest) (*pb.Task, error) {

	res, err := s.TasksServer.GetTask(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) GetTaskActivity(ctx context.Context, in *pb.GetTaskActivityRequest) (*pb.GetTaskActivityResponse, error) {

	res, err := s.TasksServer.GetTaskActivity(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) DeleteTask(ctx context.Context, in *pb.DeleteTaskRequest) (*empty.Empty, error) {


	task, err := s.str.GetTask(ctx, []string{
		string(pb.Task_Title),
	}, pb.TaskIdEq{Id: in.Id})
	if err != nil {
		return nil, err
	}

	res, err := s.TasksServer.DeleteTask(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(in.Id),
		Data:            task,
		EventName:       ".saastack.tasks.v1.Tasks.DeleteTask",
		ActivityId:      in.Id,
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil

}

func (s *logsTasksServer) UpdateTask(ctx context.Context, in *pb.UpdateTaskRequest) (*pb.Task, error) {

	task, err := s.str.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.GetTask().GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.TasksServer.UpdateTask(ctx, in)
	if err != nil {
		return nil, err
	}

	masks := pb.TaskObjectCompare(task, res, "")
	objMask := []string{}
	for _, oldMask := range masks {
		if strings.Contains(oldMask, "title") || strings.Contains(oldMask, "datetime")|| strings.Contains(oldMask, "status") {
			continue
		}
		objMask = append(objMask, oldMask)
	}

	new := &pb.Task{}
	if err = new.Update(res, objMask); err != nil {
		return nil, err
	}
	old := &pb.Task{}
	if err = old.Update(task, objMask); err != nil {
		return nil, err
	}

	updateProjectLog := map[string]*pb.Task{"new":new,"old":old}
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(res.GetId()),
		Data:            updateProjectLog,
		EventName:       ".saastack.projects.v1.Projects.UpdateTask",
		ActivityId:      res.GetId(),
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        make(map[string]string, 0),
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil

}

func (s *logsTasksServer) BatchGetTask(ctx context.Context, in *pb.BatchGetTaskRequest) (*pb.BatchGetTaskResponse, error) {

	res, err := s.TasksServer.BatchGetTask(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) ListTask(ctx context.Context, in *pb.ListTaskRequest) (*pb.ListTaskResponse, error) {

	res, err := s.TasksServer.ListTask(ctx, in)
	if err != nil {
		return nil, err
	}


	return res, nil
}

func (s *logsTasksServer) ListTasksByTitle(ctx context.Context, in *pb.ListTasksByTitleRequest) (*pb.ListTasksByTitleResponse, error) {

	res, err := s.TasksServer.ListTasksByTitle(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) ListTasksByStatus(ctx context.Context, in *pb.ListTasksByStatusRequest) (*pb.ListTasksByStatusResponse, error) {

	res, err := s.TasksServer.ListTasksByStatus(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) ListTasksByDatetime(ctx context.Context, in *pb.ListTasksByDatetimeRequest) (*pb.ListTasksByDatetimeResponse, error) {

	res, err := s.TasksServer.ListTasksByDatetime(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) AssignEmployeeCheck(ctx context.Context, in *pb.AssignEmployeeCheckRequest) (*pb.AssignEmployeeCheckResponse, error) {

	res, err := s.TasksServer.AssignEmployeeCheck(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) ListReportByDate(ctx context.Context, in *pb.ListReportByDateRequest) (*pb.ListReportByDateResponse, error) {

	res, err := s.TasksServer.ListReportByDate(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}
